local create = function(card)
	card.name = "Mega Lazor"
	card.type = "Module"
	card.cost = "Test"
	card.errata = "Deals over 9000 damage to a ship, destroys all of its modules and kills all of the crew. You win the game."
	return card
end

return create({})