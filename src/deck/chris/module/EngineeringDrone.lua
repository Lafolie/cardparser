local create = function(card)
card.name = "Engineering Drone"
card.type = "Module"
card.cost = ""
card.errata = "Startup:EngEngEngEng Maintain:EngEng \n Stats:EngEngEng"
return card
end
return create({})