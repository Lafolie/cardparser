local create = function(card)
card.name = "Repair Drone"
card.type = "Drone"
card.cost = ""
card.errata = "Startup:EngEngEng  Maintain:Eng Active: If your ship has 10 health or below increase its health by 2 each turn"
return card
end
return create({})