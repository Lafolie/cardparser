local create = function(card)
card.name = "Shields"
card.type = "Module"
card.cost = "EngEng"
card.errata = "Reactive: Block a total of 5 damage this turn."
return card
end
return create({})