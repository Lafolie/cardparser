local create = function(card)
card.name = "Rescue Mission"
card.type = "Dispose"
card.cost = "EngEngNavNavWepWep"
card.errata = "Search your databank for a crew card, show it to the other players. Put it in your hand then shuffle your databank."
return card
end
return create({})