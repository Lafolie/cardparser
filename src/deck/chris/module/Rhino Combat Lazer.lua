local create = function(card)
card.name = "Rhino Combat Lazer"
card.type = "Module"
card.cost = "EngEngWepWep"
card.errata = "Deal X Damage for each counter that is removed from the card. Each turn the module is activated add a counter to this card."
return card
end
return create({})