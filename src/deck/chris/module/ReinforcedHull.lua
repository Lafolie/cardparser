local create = function(card)
card.name = "Reinforced Hull"
card.type = "Dispose"
card.cost = "EngEngEngEngEng"
card.errata = "Gain 5 Health"
return card
end
return create({})