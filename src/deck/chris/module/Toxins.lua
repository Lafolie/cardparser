local create = function(card)
card.name = "Neurotoxin"
card.type = "Disposable"
card.cost = "MedMedSciWepWep"
card.errata = "Remove 1 Crew"
return card
end
return create({})