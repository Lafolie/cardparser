local create = function(card)
card.name = "Nano Defibrillator"
card.type = "Disposable"
card.cost = "MedMedMedMed"
card.errata = "Revive a crew member"
return card
end
return create({})