local create = function(card)
card.name = "Pegasus Engine"
card.type = "Module"
card.cost = "EngEngNavNav"
card.errata = "Reactive: Reduse all damage by 1 this turn"
return card
end
return create({})