local create = function(card)
card.name = "Landing Party"
card.type = "Disposable"
card.cost = "EngEngWepWepNav"
card.errata = "Search your databank for a crew card, show it to the other players. Put it in your hand then shuffle your databank"
return card
end
return create({})