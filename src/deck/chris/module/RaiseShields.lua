local create = function(card)
card.name = "Reinforced Hull"
card.type = "Dispose"
card.cost = "EngEngEngNav"
card.errata = "Stop an enemy attack"
return card
end
return create({})