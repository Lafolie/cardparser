local create = function(card)
card.name = "Unstable Lazer"
card.type = "Module"
card.cost = "Eng"
card.errata = "Energy Deal 1 damage. If this card is not activated for 3 turns take 3 damage"
return card
end
return create({})