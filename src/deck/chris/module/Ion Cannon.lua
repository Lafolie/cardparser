local create = function(card)
card.name = "Ion Cannon"
card.type = "Module"
card.cost = "EngWepWep"
card.errata = "Disable 1 Module"
return card
end
return create({})