local create = function(card)
card.name = "Defence Formation"
card.type = "Module"
card.cost = "NavNav"
card.errata = "Reactive: Block 1 damage from each attack made this turn"
return card
end
return create({})