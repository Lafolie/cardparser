local create = function(card)
card.name = "Caffine Boost"
card.type = "Disposable"
card.cost = "MedMed"
card.errata = "A crew member may be used twice this turn"
return card
end
return create({})