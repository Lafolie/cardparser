local create = function(card)
	card.name = "Cellular Rootkit"
	card.type = "Augment"
	card.cost = "SciSciEngWepWep"
	card.errata = "Augment module:\n\tAugmented Module is Organic.\nDamage dealt by augmented Module is reduced by 1."
	return card
end

return create({})