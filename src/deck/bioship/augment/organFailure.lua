local create = function(card)
	card.name = "Organ Failure"
	card.type = "Augment"
	card.cost = "SciMedWep"
	card.errata = "Augment organic module:\n\tWhen the augmented Module is activated, destroy it (after resolving its effects)."
	return card
end

return create({})