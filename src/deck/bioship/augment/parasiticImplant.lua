local create = function(card)
	card.name = "Parasitic Implant"
	card.type = "Augment"
	card.cost = "SciSciMedMedWep"
	card.errata = "Parasitic Implant may only be played when you activate a Weapon. If the activated Weapon deals no damage to its target, discard Parasitic Implant. Augment organic:\n\tAugmented card cannot be activated. When Parasitic Implant is destroyed you gain 3HP."
	return card
end

return create({})