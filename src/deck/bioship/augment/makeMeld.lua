local create = function(card)
	card.name = "Nervous Chamber"
	card.type = "Augment"
	card.cost = "SciMedMedEng"
	card.errata = "Augment organic module:\n\tAugmented module has Meld."
	return card
end

return create({})