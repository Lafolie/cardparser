local create = function(card)
	card.name = "Neural Targeting"
	card.type = "Augment"
	card.cost = "SciSciMedMedSftSft"
	card.errata = "Augment organic module:\n\tDamage caused by augmented module is increased by 1."
	return card
end

return create({})