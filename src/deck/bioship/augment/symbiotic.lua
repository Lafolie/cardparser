local create = function(card)
	card.name = "Symbiotic Capacitor"
	card.type = "Augment"
	card.cost = "SciMedEng"
	card.errata = "Augment module:\n\tAugmented module is organic and any damage dealt by it is increased by 2. At the start of your turn you lose 1HP."
	return card
end

return create({})