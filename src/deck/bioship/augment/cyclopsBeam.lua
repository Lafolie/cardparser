local create = function(card)
	card.name = "Occular Beam"
	card.type = "Augment"
	card.cost = "SciSciSciSciMedMedWepWep"
	card.errata = "Augment organic module:\n\tAugmented module has Meld and its Active becomes \"As long as this module is melded with a crew member, deal 3 damage to an opponent's ship.\""
	return card
end

return create({})