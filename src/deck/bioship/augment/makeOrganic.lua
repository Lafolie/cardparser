local create = function(card)
	card.name = "Desecrate"
	card.type = "Augment"
	card.cost = "SciSciMedEng"
	card.errata = "Augment module:\n\tAugmented Module is Organic."
	return card
end

return create({})