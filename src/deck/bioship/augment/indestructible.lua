local create = function(card)
	card.name = "Silicon-Fibre Tissue"
	card.type = "Augment"
	card.cost = "SciSciMedMedEng"
	card.errata = "Augment organic:\n\tAugmented card cannot be destroyed."
	return card
end

return create({})