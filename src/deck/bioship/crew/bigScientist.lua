local create = function(card)
	card.name = "Natural Philosopher"
	card.type = "Crew"
	card.cost = ""
	card.errata = "Science 4"
	return card
end

return create({})