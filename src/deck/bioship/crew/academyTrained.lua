local create = function(card)
	card.name = "Academy Alumni"
	card.type = "Crew"
	card.cost = ""
	card.errata = "Navigation 2\nEngineering 1\nWeapons 1"
	return card
end

return create({})