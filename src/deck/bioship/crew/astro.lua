local create = function(card)
	card.name = "Astro Physicist"
	card.type = "Crew"
	card.cost = ""
	card.errata = "Navigation 2\nScience 2"
	return card
end

return create({})