local create = function(card)
	card.name = "Flight Assistant"
	card.type = "Crew"
	card.cost = ""
	card.errata = "Navigation 2\nSoftware 1\nScience 1"
	return card
end

return create({})