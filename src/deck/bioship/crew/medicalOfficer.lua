local create = function(card)
	card.name = "Medical Officer"
	card.type = "Crew"
	card.cost = ""
	card.errata = "Medical Science 3\nScience 1"
	return card
end

return create({})