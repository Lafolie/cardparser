local create = function(card)
	card.name = "Military Hire"
	card.type = "Crew"
	card.cost = ""
	card.errata = "Weapons 2\nEngineering 2"
	return card
end

return create({})