local create = function(card)
	card.name = "Convicted Hacker"
	card.type = "Crew"
	card.cost = ""
	card.errata = "Software 3\nWeapons 1"
	return card
end

return create({})