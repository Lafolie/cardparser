local create = function(card)
	card.name = "Rocket Scientist"
	card.type = "Crew"
	card.cost = ""
	card.errata = "Science 3\nSoftware 1"
	return card
end

return create({})