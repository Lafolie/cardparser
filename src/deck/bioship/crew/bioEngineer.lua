local create = function(card)
	card.name = "Bioengineer"
	card.type = "Crew"
	card.cost = ""
	card.errata = "\nEngineering 2\nMedical Science 1\nScience 1"
	return card
end

return create({})