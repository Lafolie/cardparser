local create = function(card)
	card.name = "Secretion Shield"
	card.type = "Module"
	card.cost = "SciMedEng"
	card.errata = "Organic, Reactive\nAt the start of your turn you gain 1HP as long as your HP is less than 10.\nActive:\n\tPrevent the next 1 point of damage received."
	return card
end

return create({})