local create = function(card)
	card.name = "Flight Deck"
	card.type = "Module"
	card.cost = "NavNav"
	card.errata = "Reactive\nActive:\n\tPrevent the next 2 points of damage received."
	return card
end

return create({})