local create = function(card)
	card.name = "Living Hardpoint"
	card.type = "Module"
	card.cost = ""
	card.errata = "Organic\nLiving Hardpoint can not be targeted by opponents unless it is augmented."
	return card
end

return create({})