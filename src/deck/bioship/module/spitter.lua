local create = function(card)
	card.name = "Synaptic Laser"
	card.type = "Module"
	card.cost = "SciSciWep"
	card.errata = "Organic, Weapon\nActivate:\n\tDeal 1 damage to an opponent's ship."
	return card
end

return create({})