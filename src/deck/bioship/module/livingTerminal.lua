local create = function(card)
	card.name = "Encephalic Terminal"
	card.type = "Module"
	card.cost = "SciSciSft"
	card.errata = "Organic\nActivate:\n\tLook at the top card in your databank. You may put it on the bottom of your databank."
	return card
end

return create({})