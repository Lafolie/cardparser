local create = function(card)
	card.name = "Name of Card"
	card.type = "Module, Crew, etc"
	card.cost = "Activation Cost"
	card.errata = "Card Rules."
	return card
end

return create({})