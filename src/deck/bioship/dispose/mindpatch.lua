local create = function(card)
	card.name = "Mind Patch"
	card.type = "Dispose"
	card.cost = "SciSciMedMed"
	card.errata = "Discard a Crew card. Until your next turn a single crew member gains all skills of the discarded card (in addition to their own)."
	return card
end

return create({})