local create = function(card)
	card.name = "Epinephrine Shot"
	card.type = "Dispose"
	card.cost = "MedMedSci"
	card.errata = "Until the end of the turn target friendly crew member provides 1 extra point in all their skills."
	return card
end

return create({})