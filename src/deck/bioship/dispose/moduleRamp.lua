local create = function(card)
	card.name = "Bioscan"
	card.type = "Dispose"
	card.cost = "SciMedSftSft"
	card.errata = "Search your databank for an organic module, show it, then put it in your hand. Shuffle your databank."
	return card
end

return create({})