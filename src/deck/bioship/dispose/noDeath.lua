local create = function(card)
	card.name = "Refuse To Die"
	card.type = "Dispose"
	card.cost = "NavNavNav"
	card.errata = "As long as your HP is 3 or lower, prevent all damage dealt to your ship until the end of the turn."
	return card
end

return create({})