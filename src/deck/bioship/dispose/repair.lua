local create = function(card)
	card.name = "Immediate Repair"
	card.type = "Dispose"
	card.cost = "EngEngEng"
	card.errata = "Destroy an Augment attached to one of your Modules."
	return card
end

return create({})