local create = function(card)
	card.name = "Reconstitute"
	card.type = "Dispose"
	card.cost = "SciSciEng"
	card.errata = "Choose a card from your discard pile and return it to your hand."
	return card
end

return create({})