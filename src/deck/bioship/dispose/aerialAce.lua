local create = function(card)
	card.name = "Aerial Ace"
	card.type = "Dispose"
	card.cost = "NavNavNav"
	card.errata = "Prevent the next instance of damage received."
	return card
end

return create({})