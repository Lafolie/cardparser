local create = function(card)
	card.name = "Osmose Module"
	card.type = "Dispose"
	card.cost = "SciSciMedMedEng"
	card.errata = "Destroy one of your modules to gain 5HP."
	return card
end

return create({})