--[[
	Card
	Manages and creates a canvas based on table input.
]]

--pseudo constants
local black = {0, 0, 0, 255}
local white = {255, 255, 255, 255}

local anchor = {
	name = {x = 35, y = 35},
	type = {x = 35, y = 596},
	cost = {x = 281, y = 596},
	errata = {x = 35, y = 685}
}

local skillList = {
	"Eng",
	"Sci",
	"Wep",
	"Sft",
	"Nav",
	"Med"
}

error = "Error"

class "Card" {
	__init__ = function(self, data)
		data = dirHack .. '/' .. data
		if pcall(self.loadFile, data) then
			self.data = self.loadFile(data)
		else
			
			self.data = {
							name = error,
							type = error,
							cost = error,
							errata = error
						}
			print("Error detected in " .. data .. "!!!")
		end

		self.canvas = theUltimateCanvas
		self:drawBG()
		self:drawInfo()
		self:savePng()
		cardCount = cardCount + 1
	end,

	drawBG = function(self)
		self.canvas:renderTo(function()
			love.graphics.setColor(white)
			love.graphics.draw(background, 0, 0)
		end)
	end,

	drawInfo = function(self)
		self.canvas:renderTo(function()
			love.graphics.setColor(black)
			for property, str in pairs(self.data) do
				local pos = anchor[property]
				if str and property ~= "errata" then
					if property == "cost" then
						love.graphics.print(self.getCostString(str), pos.x, pos.y)
					else
						love.graphics.print(str, pos.x, pos.y)
					end
				elseif str then
					love.graphics.printf(str, pos.x, pos.y, 590, "left")
				end
			end
		end)
	end,

	savePng = function(self)
		if self.data.name ~= error then
			--absolute filth
			local img = self.canvas:getImageData()
			img:encode(deckName .. "/" .. self.data.name .. ".png")
			self:updatePage()
			collectgarbage("collect")
			print("Saved " .. self.data.name .. ".png")
		end
	end,

	loadFile = function(file)
		return love.filesystem.load(file)()
	end,

	updatePage = function(self)
		canvasA4:renderTo(function()
			love.graphics.setColor(white)
			love.graphics.draw(self.canvas, grid.x * (self.canvas:getWidth() + 125) + 150, grid.y * (self.canvas:getHeight() + 125) + 75)
		end)
		if grid.x == 2 then
			grid.x = 0
			grid.y = grid.y + 1
		else
			grid.x = grid.x + 1
		end
		if grid.y == 3 then
			local sheet = canvasA4:getImageData()
			sheet:encode(deckName .. "/sheet" .. string.format("%02i", page) .. ".png")
			print("Saved sheet" .. string.format("%02i", page) .. ".png")
			page = page + 1
			grid.y = 0
			canvasA4:renderTo(function()
				love.graphics.setColor(white)
				love.graphics.rectangle("fill", 0, 0, 2480, 3508)
			end)
		end
	end,

	getCostString = function(str)
		local result = ""
		for k, skill in ipairs(skillList) do
			local _, count = str:gsub(skill, "%1")
			if count > 0 then
				result = result .. count .. skill .. " "
			end
		end
		return result
	end

}