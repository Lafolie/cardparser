--do the loadings
class = require "lib.slither"
require "class.card"

--temporary
deckName = "chris"
love.filesystem.createDirectory(deckName)
--everything in love.load rawwwwwrrrrr!
love.load = function()
	print "Loading...\nMax Canvas Size:"
	startTime = love.timer.getTime()
	print(love.graphics.getSystemLimit("texturesize"))
	--filthy globals
	love.graphics.setFont(love.graphics.newFont(32))
	background = love.graphics.newImage("gfx/background.png")
	cardType = {"crew", "module", "augment", "drone", "dispose"}
	theUltimateCanvas = love.graphics.newCanvas(background:getWidth(), background:getHeight())
	canvasA4 = love.graphics.newCanvas(2480, 3508)
	canvasA4:renderTo(function()
		love.graphics.setColor(255, 255, 255, 255)
		love.graphics.rectangle("fill", 0, 0, 2480, 3508)
	end)
	cardCount = 0
	page = 1
	grid = {x = 0, y = 0}
	for _, dir in ipairs(cardType) do
		print("Scanning " .. dir .. "...")
		dirHack = "deck/" .. deckName .. "/" .. dir
		love.filesystem.getDirectoryItems(dirHack, renderCard) --instantiate a class as function argument. Only in Lua. Probably.
	end
	print("Finished scanning directories.")
	if not(grid.x == 0 and grid.y == 0) then
		local sheet = canvasA4:getImageData()
		sheet:encode(deckName .. "/sheet" .. string.format("%02i", page) .. ".png")
		print("Saved sheet" .. string.format("%02i", page) .. ".png")
	end
	timeTaken = love.timer.getTime() - startTime
	print("Time elapsed", timeTaken)
	print("Total Card #", cardCount)

	--love.event.push("quit")
end

love.update = function(dt)
	--parse so many cards per frame? NAY! do it in load :)
end

love.draw = function()
	love.graphics.print("Done.\nPress any key to quit.", 10, 10)
end

love.keypressed = function(key, unicode)
	--quit
	love.event.push("quit")
end

renderCard = function(file)
	Card(file)
end